from flask import render_template
from gevent.pywsgi import WSGIServer

import config
from cou_forums import app
from cou_forums.controllers.api import api
from cou_forums.controllers.api.auth import auth as auth_api
from cou_forums.controllers.api.deletions import deletions as deletions_api
from cou_forums.controllers.api.submissions import submissions as submissions_api
from cou_forums.controllers.api.voting import voting as voting_api
from cou_forums.controllers.api.watching import watching as watching_api
from cou_forums.controllers.errors import lyric
from cou_forums.controllers.views import views

app.register_blueprint(api)
app.register_blueprint(auth_api)
app.register_blueprint(deletions_api)
app.register_blueprint(submissions_api)
app.register_blueprint(voting_api)
app.register_blueprint(watching_api)
app.register_blueprint(views)


@app.errorhandler(404)
def handle_404(e):
    return render_template("404.html", error=e, lyric=lyric()), 404


if __name__ == "__main__":
    http_server = WSGIServer((str(), config.port), app)
    print(f"Forums server v3 running on port {http_server.server_port}.")
    http_server.serve_forever()
