from datetime import datetime

from flask import Flask, redirect, session
from flask_mail import Mail

import config

# Configure Flask
app = Flask(__name__)
app.secret_key = config.secret_key

# Configure email extension
app.config["mail_server"] = config.mail_server
app.config["mail_username"] = config.mail_username
app.config["mail_password"] = config.mail_password
mail = Mail(app)


@app.context_processor
def inject_year():
    return {"year": datetime.now().year}


@app.context_processor
def inject_login():
    data = {"logged_in": "auth" in session}

    if data["logged_in"]:
        data["logged_in_username"] = session["auth"]["playerName"]
        data["logged_in_user_id"] = session["user_id"]

    return data


@app.route("/packages/cou_login/cou_login/assets/logo.svg")
def cou_login_logo_request():
    return redirect("/static/img/cou_ur.svg", 301)
