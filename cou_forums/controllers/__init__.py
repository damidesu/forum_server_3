from functools import wraps
import re
import json

from flask import jsonify, request, Response, session
from sqlalchemy.orm.exc import NoResultFound

from cou_forums.data import db_connect
from cou_forums.data.user import User


def remove_script_tags(html: str) -> str:
    return re.sub(r'</?script[^>]*?>', str(), html)


def require_auth(f):
    """Aborts the request with an error if the user is not logged in."""

    @wraps(f)
    def decorated_function(*args, **kwargs):
        # Make sure the user is logged in
        if "auth" not in session:
            return respond(False, "not logged in"), 401

        # Make sure the user is a real user
        user_id = session["user_id"]
        db = db_connect()
        try:
            db.query(User).filter(User.user_id == user_id).one()
        except NoResultFound:
            return respond(False, "user not found"), 401

        return f(*args, **kwargs)

    return decorated_function


def require_post_id(f):
    """
    Aborts the reponse with an error if the post id is not provided or
    is not an integer.
    """

    @wraps(f)
    def decorated_function(*args, **kwargs):
        post_id = json.loads(request.data).get("post_id")

        if not post_id:
            return respond(False, "missing post_id"), 400

        return f(*args, **kwargs)

    return decorated_function


def require_reply_id(f):
    """
    Aborts the response with an error if the reply id is not provided or
    is not an integer.
    """

    @wraps(f)
    def decorated_function(*args, **kwargs):
        reply_id = json.loads(request.data).get("reply_id")

        if not reply_id:
            return respond(False, "missing reply_id"), 400

        return f(*args, **kwargs)

    return decorated_function


def respond(ok: bool = True, data: str = str()) -> Response:
    """
    :param ok: Whether the request was successful.
    :param data: Additional status information.
    :return: A Flask response.
    """
    return jsonify({"result": "OK" if ok else "ERROR", "data": data})
