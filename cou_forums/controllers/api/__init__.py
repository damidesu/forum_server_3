import base64
import json
from urllib.parse import quote
from urllib.request import urlopen

from flask import Blueprint, request, Response, session

import config
from cou_forums.controllers import (
    require_auth,
    require_post_id
)
from cou_forums.data import db_connect
from cou_forums.data.post import Post

GAME_SERVER = f"{config.game_server_uri}:8181"
api = Blueprint("api", __name__, url_prefix="/api")


@api.route("/avatar_head/<username>.png")
def avatar_head(username):
    username = quote(username)
    with urlopen(f"{GAME_SERVER}/trimImage?username={username}") as url:
        image_str = url.read()
    image = base64.b64decode(image_str)
    return Response(image, mimetype="image/png", headers={
        "Cache-Control": "public, max-age=604800, s-maxage=86400",
    })


@api.route("/watch", methods=["POST"])
@require_auth
@require_post_id
def watch():
    post_id = json.loads(request.data).get("post_id", type=int)
    db = db_connect()
    post = db.query(Post).filter(Post.entry_id == post_id).one()
    post.add_watcher(session["user"])
    db.commit()
    db.close()


@api.route("/unwatch", methods=["POST"])
@require_auth
@require_post_id
def unwatch():
    post_id = json.loads(request.data).get("post_id", type=int)
    db = db_connect()
    post = db.query(Post).filter(Post.entry_id == post_id).one()
    post.remove_watcher(session["user"])
    db.commit()
    db.close()
