import json

from flask import Blueprint, request, session
from sqlalchemy.orm.exc import NoResultFound

from cou_forums.controllers import (
    respond,
    require_auth,
    require_post_id,
    require_reply_id,
)
from cou_forums.data import db_connect
from cou_forums.data.reply import Reply
from cou_forums.data.post import Post

deletions = Blueprint("deletions", __name__, url_prefix="/api/delete")


@deletions.route("/post", methods=["POST"])
@require_auth
@require_post_id
def delete_post():
    data = json.loads(request.data)
    post_id = data.get("post_id")

    db = db_connect()
    try:
        post = db.query(Post).filter(Post.entry_id == post_id).one()
        url = post.category.url
    except NoResultFound:
        db.close()
        return respond(ok=False, data="Post not found. Was it already deleted?")

    if session["user_id"] != post.user_id:
        db.close()
        return respond(ok=False, data="cannot delete other user's post"), 403

    db.delete(post)
    db.commit()
    db.close()
    return respond(data=url)


@deletions.route("/reply", methods=["POST"])
@require_auth
@require_reply_id
def delete_reply():
    data = json.loads(request.data)
    reply_id = data.get("reply_id")

    db = db_connect()
    try:
        reply = db.query(Reply).filter(Reply.entry_id == reply_id).one()
    except NoResultFound:
        db.close()
        return respond(ok=False, data="Reply not found. Was it already deleted?")

    if session["user_id"] != reply.user_id:
        db.close()
        return respond(ok=False, data="cannot delete other user's reply"), 403

    db.delete(reply)
    db.commit()
    db.close()
    return respond()
