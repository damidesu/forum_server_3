import json

from flask import Blueprint, request, session
from sqlalchemy.orm.exc import NoResultFound

from cou_forums.controllers import respond, require_auth, remove_script_tags
from cou_forums.controllers.api.watching import notify_watchers
from cou_forums.data import db_connect
from cou_forums.data.category import Category
from cou_forums.data.post import Post
from cou_forums.data.reply import Reply

submissions = Blueprint("submissions", __name__, url_prefix="/api/submit")


@submissions.route("/post", methods=["POST"])
@require_auth
def submit_post():
    data = json.loads(request.data)
    title = data.get("title")

    if not title:
        return respond(ok=False, data="post title cannot be blank"), 400

    category_id = data.get("category") or None
    post_id = data.get("post_id") or None
    content = remove_script_tags(data.get("content") or str())
    db = db_connect()

    if post_id:
        # Editing post
        try:
            post = db.query(Post).filter(Post.entry_id == post_id).one()
        except NoResultFound:
            return respond(ok=False, data="no such post"), 400

        post.title = title
        post.content = content
        post.edited_now()
        db.add(post)
    else:
        # New post
        if not Category.find(category_id):
            return respond(ok=False, data="invalid category"), 400

        post = Post(category_id, title, content, session["user_id"])
        post.created_now()
        db.add(post)

    db.commit()
    url = post.url
    db.close()
    return respond(data=url)


@submissions.route("/reply", methods=["POST"])
@require_auth
def submit_reply():
    data = json.loads(request.data)
    reply_id = data.get("reply_id")
    post_id = data.get("post_id")
    content = remove_script_tags(data.get("content"))

    if not content:
        return respond(ok=False, data="missing content"), 400

    if not reply_id and not post_id:
        return respond(ok=False, data="missing post id"), 400

    db = db_connect()

    if reply_id:
        # Editing reply
        try:
            reply = db.query(Reply).filter(Reply.entry_id == reply_id).one()
        except NoResultFound:
            db.close()
            return respond(ok=False, data="reply not found")

        reply.content = content
        reply.edited_now()
        db.add(reply)
    else:
        # New reply
        try:
            db.query(Post).filter(Post.entry_id == post_id).one()
        except NoResultFound:
            db.close()
            return respond(ok=False, data="post not found"), 400

        reply = Reply(content, post_id, session["user_id"])
        reply.created_now()
        db.add(reply)

    db.commit()
    url = reply.url

    if not reply_id:
        # Send new reply notifications to post watchers
        notify_watchers(reply)

    db.close()
    return respond(data=url)
