from math import ceil

from flask import Blueprint, render_template, abort, request, redirect, session
from sqlalchemy import and_, or_, func
from sqlalchemy.orm.exc import NoResultFound

from cou_forums.data import db_connect
from cou_forums.data.category import (
    Category,
    POST_LIST_LIMIT,
    CATEGORIES,
    PAGE as CATEGORY_PATH,
)
from cou_forums.data.post import Post, PAGE as THREAD_PATH
from cou_forums.data.reply import Reply
from cou_forums.data.user import User, PAGE as USER_PATH

views = Blueprint("views", __name__)


@views.route("/")
def home():
    """Show the forums homepage."""
    return render_template("index.html", categories=CATEGORIES)


@views.route("/login")
def login():
    """Show the login page."""
    return render_template("login.html")


@views.route("/logout")
def logout_api():
    session.pop("auth")
    return redirect(request.referrer)


@views.route(f"/{USER_PATH}/<username>")
def user(username):
    """Show a user's forums data."""
    if not username:
        return abort(404)

    db = db_connect()

    try:
        global found_user
        found_user = (
            db.query(User).filter(func.lower(User.username) == username.lower()).one()
        )

        if found_user.username != username:
            # Capitalization was incorrect
            return redirect(found_user.url, code=301)
    except NoResultFound:
        db.close()
        return abort(404)

    posts = (
        db.query(Post)
        .order_by(Post.created.desc())
        .filter(Post.user_id == found_user.user_id)
        .all()
    )
    replies = (
        db.query(Reply)
        .order_by(Reply.created.desc())
        .filter(Reply.user_id == found_user.user_id)
        .all()
    )
    db.close()

    return render_template("user.html", user=found_user, posts=posts, replies=replies)


@views.route(f"/{CATEGORY_PATH}/<cat_id>/new")
def new_post(cat_id):
    cat = Category.find(cat_id)
    if not cat:
        return abort(404)

    return render_template("edit.html", mode="post", entry=None, category=cat)


@views.route(f"/{CATEGORY_PATH}/<cat_id>")
@views.route(f"/{CATEGORY_PATH}/<cat_id>/<page>")
def category(cat_id, page=None):
    """Show the threads posted in a given category."""
    # Check category
    cat = Category.find(cat_id)
    if not cat:
        return abort(404)

    # Default page number
    if not page:
        return redirect(cat.url, code=301)

    # Check page number
    if not str.isdigit(page):
        return abort(404)

    page = int(page)
    num_pages = ceil(cat.num_posts / POST_LIST_LIMIT)

    # Page number out of bounds
    if page > num_pages:
        return abort(404)

    db = db_connect()
    posts = (
        db.query(Post)
        .order_by(Post.created.desc())
        .filter(Post.category_id == cat.cat_id)
        .limit(POST_LIST_LIMIT)
        .offset(POST_LIST_LIMIT * (page - 1))
    )
    db.close()
    return render_template(
        "category.html", category=cat, posts=posts, page=page, num_pages=num_pages
    )


@views.route(f"/{THREAD_PATH}/<post_id>")
def thread(post_id):
    """Show a post and its replies."""
    if not str.isdigit(post_id):
        return abort(404)

    db = db_connect()
    try:
        post = db.query(Post).filter(Post.entry_id == post_id).one()
    except NoResultFound:
        return abort(404)
    finally:
        db.close()
    return render_template("thread.html", post=post)


@views.route(f"/{THREAD_PATH}/edit/<post_id>")
def edit_post(post_id):
    if not str.isdigit(post_id):
        return abort(404)

    db = db_connect()
    try:
        post = db.query(Post).filter(Post.entry_id == post_id).one()
    except NoResultFound:
        return abort(404)
    finally:
        db.close()
    return render_template("edit.html", mode="post", entry=post, category=post.category)


@views.route(f"/reply/edit/<reply_id>")
def edit_reply(reply_id):
    if not str.isdigit(reply_id):
        return abort(404)

    db = db_connect()
    try:
        reply = db.query(Reply).filter(Reply.entry_id == reply_id).one()
    except NoResultFound:
        return abort(404)
    finally:
        db.close()
    return render_template("edit.html", mode="reply", entry=reply)


@views.route(f"/search")
def search():
    """Show the matching posts for a search query."""
    query = request.args.get("q", type=str, default=str()).lower()
    posts = []

    if query:
        db = db_connect()
        posts = (
            db.query(Post)
            .filter(
                and_(
                    Post.category_id != "comments",
                    or_(
                        func.lower(Post.title).contains(query),
                        func.lower(Post.content).contains(query),
                    ),
                )
            )
            .order_by(Post.created.desc())
            .all()
        )
        db.close()

    return render_template("search.html", query=query, posts=posts)
