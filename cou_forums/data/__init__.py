from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

import config

Base = declarative_base()

engine = create_engine(config.db_uri)
db_connect = sessionmaker(bind=engine)
