from cou_forums.data import db_connect

PAGE = "category"
POST_LIST_LIMIT = 20


class Category:
    _num_posts = None

    @staticmethod
    def find(cat_id: str):
        for cat in CATEGORIES:
            if cat.cat_id == cat_id:
                return cat

        return None

    def __init__(
        self,
        name: str,
        description: str,
        color: str = "default",
        cat_id: str = str(),
        hidden: bool = False,
    ):
        self.name = name
        self.description = description
        self.color = color
        self.cat_id = cat_id if cat_id else name.replace(" ", "").lower()
        self.hidden = hidden
        self.voting_enabled = self.cat_id == "voting"

    @property
    def url(self):
        return f"/{PAGE}/{self.cat_id}/1"

    @property
    def num_posts(self) -> int:
        if not self._num_posts:
            from cou_forums.data.post import Post

            session = db_connect()
            self._num_posts = (
                session.query(Post).filter(Post.category_id == self.cat_id).count()
            )
            session.close()

        return self._num_posts


CATEGORIES = [
    Category(
        "Announcements & Events",
        "Talking about game updates as well as planning in-game and real-world events.",
        color="info",
        cat_id="anev",
    ),
    Category(
        "General",
        (
            "Whatever you want it to be "
            "(as long as it's at least tangentially related to the game)."
        ),
        color="primary",
    ),
    Category(
        "Ideas", "What do you think we should put in the game next?", color="success"
    ),
    Category(
        "Bugs",
        (
            "Discussion of possible bugs found in the game. "
            "Note that this is not for official bug reports."
        ),
        color="danger",
    ),
    Category(
        "Off Topic",
        "A place to talk about anything not related to the game.",
        color="warning",
    ),
    Category(
        "Voting",
        (
            "Replies to posts here can be voted up or down "
            "and will be ranked by the score."
        ),
    ),
    Category("Comments", "Used for comments on blog posts.", hidden=True),
]
