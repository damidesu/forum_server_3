from abc import abstractmethod
from datetime import datetime
import re
from textwrap import shorten

from sqlalchemy import Column, DateTime, Integer, String, ForeignKey
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import relationship

from cou_forums.data.user import User
from cou_forums.data.util import rel_time


class Entry:
    entry_id = Column("id", Integer, primary_key=True)
    content = Column(String, nullable=False, default=str())
    created = Column(DateTime, nullable=False, default=datetime.utcnow())
    edited = Column(DateTime, nullable=False, default=datetime.utcfromtimestamp(0))

    @declared_attr
    def user_id(self):
        return Column(Integer, ForeignKey(User.user_id), nullable=False)

    @declared_attr
    def user(self):
        return relationship(User, lazy="joined")

    @property
    def was_edited(self) -> bool:
        return self.edited > self.created

    @property
    def created_ago(self) -> str:
        return rel_time(self.created)

    @property
    def edited_ago(self) -> str:
        return rel_time(self.edited)

    def created_now(self):
        self.created = datetime.utcnow()

    def edited_now(self):
        self.edited = datetime.utcnow()

    @property
    def plaintext(self):
        return re.sub("<[^<]+?>|&[a-zA-Z]+;", str(), self.content or str())

    @property
    def preview(self):
        return shorten(self.plaintext, width=200)

    @abstractmethod
    def edit_url(self):
        pass
