import json

from sqlalchemy import Column, String
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import relationship

from cou_forums.data import Base
from cou_forums.data.entry import Entry
from cou_forums.data.reply import Reply

PAGE = "thread"


class Post(Base, Entry):
    __tablename__ = "posts"

    title = Column(String)
    category_id = Column("category", String, nullable=False)
    _watchers = Column("watchers", String, nullable=False, default="[]")

    def __init__(self, category_id, title, content, user_id):
        self.category_id = category_id
        self.title = title
        self.content = content
        self.user_id = user_id

    @property
    def watchers(self) -> set:
        return set(json.loads(self._watchers))

    @property
    def num_watchers(self) -> int:
        return len(self.watchers)

    def add_watcher(self, user_id):
        self._watchers = json.dumps(list(self.watchers | {user_id}))

    def remove_watcher(self, user_id):
        self._watchers = json.dumps(list(self.watchers - {user_id}))

    @property
    def category(self):
        from cou_forums.data.category import Category

        return Category.find(self.category_id)

    @property
    def url(self) -> str:
        return f"/{PAGE}/{self.entry_id}"

    @declared_attr
    def replies(self):
        return relationship(Reply, lazy="joined", cascade="delete")

    @property
    def num_replies(self) -> int:
        return len(self.replies)

    @property
    def edit_url(self):
        return f"/{PAGE}/edit/{self.entry_id}"
