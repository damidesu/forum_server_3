from sqlalchemy import Column, DateTime, Integer, String

from cou_forums.data import Base

PAGE = "user"
PROFILE_PAGE = "https://childrenofur.com/profile"


class User(Base):
    __tablename__ = "users"

    user_id = Column("id", Integer, primary_key=True)
    username = Column(String)
    email = Column(String)
    bio = Column(String)
    registration_date = Column(DateTime)
    elevation = Column(String)

    @property
    def url(self):
        return f"/{PAGE}/{self.username}"

    @property
    def profile_url(self):
        return f"{PROFILE_PAGE}?username={self.username}"

    @property
    def avatar_url(self):
        return f"/api/avatar_head/{self.username}.png"
